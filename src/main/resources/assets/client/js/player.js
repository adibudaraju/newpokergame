var app = new Vue({
  el: '#app',
  data: {
    // Player private state
    name: '',
    money: 0,
    totalMoneyInPot: 0,
    toStay: 0,
    cardFileUris: [
        '/assets/client/png/cards/red_joker.png',
        '/assets/client/png/cards/black_joker.png'
    ],
    communityCardUris: [],
    gameStateSequenceNumber: 0,
    stateChangeMessages: [],
    whoHasTheTurn: '',
    dealer: '',
    isOnTurn: false,
    isDealer: false,
    isAllIn: false,
    hasFolded: false,
    turnChoice: 'bet',
    betAmount: 0,
    message: '',

    // All players, and their public states
    players: []
  },
  methods: {
    playTurn: function(event) {
       var that = this
       if (this.turnChoice == "bet") {
           axios.post('/api/player/bet', { money: that.betAmount })
               .then(function(response) {
                   var respData = response.data
                   if (respData.success) {
                       that.message = 'Bet placed'
                   } else {
                       that.message = respData.error
                   }
               })
               .catch(function(error) {
                   that.message = 'Unable to send message to server, refresh page & try again'
               })
            return
        }

        if (this.turnChoice == 'fold') {
            axios.post('/api/player/fold')
                .then(function(response) {
                   var respData = response.data
                   if (respData.success) {
                       that.message = 'Folded'
                   } else {
                       that.message = respData.error
                   }
                })
               .catch(function(error) {
                   that.message = 'Unable to send message to server, refresh page & try again'
               })
           return
        }

        alert('Choice not implemented')
    },

    nextPlayer: function() {
        that = this
        axios.post('/api/player/next')
            .then(function(response) {
               var respData = response.data
               if (respData.success) {
                   that.message = 'Turn moved to next player'
               } else {
                   that.message = respData.error
               }
            })
           .catch(function(error) {
               that.message = 'Unable to send message to server, refresh page & try again'
           })
    },

    showCards: function() {
            that = this
            axios.post('/api/player/show')
                .then(function(response) {
                   var respData = response.data
                   if (respData.success) {
                       that.message = 'Cards successfully shown through action log'
                   } else {
                       that.message = respData.error
                   }
                })
               .catch(function(error) {
                   that.message = 'Unable to send message to server, refresh page & try again'
               })
    },

    assumeRole: function(player) {
        that = this
        setPlayerSecretToken(player.cookieDuringFakeGame)
        const reloadPauseSec = 2
        this.message = "Assumed role as " + player.name + ", page will refresh in " +
            reloadPauseSec + " seconds"
        window.setTimeout(function() { location.reload() }, reloadPauseSec * 1000)
    }
  }
})

var numConsecutiveErrors = 0
function longPoll() {
    axios.get('/api/player/poll/' + app.gameStateSequenceNumber)
        .then(function(response) {
            var pollResp = response.data
            if (!pollResp.success) {
                // Server error - user should refresh
                app.message = 'Error on server, please refresh: ' + pollResp.error
                return
            }

            // For fake games, the cookie is set on the first poll call
            if (app.gameStateSequenceNumber == 0 && pollResp.isFakeGame && !!pollResp.playerSecretCookieIfFakeGame) {
                var playerSecretToken = pollResp.playerSecretCookieIfFakeGame
                setPlayerSecretToken(playerSecretToken)
                app.message = 'Player secret token allocated for fake game'
            }

            if (app.gameStateSequenceNumber == pollResp.gameStateCounter) {
                // Game state did not change, just resume the long poll request
                longPoll()
                return
            }

            // Game state did change - do a refresh
            app.gameStateSequenceNumber = pollResp.gameStateCounter
            axios.get('/api/player/state')
                .then(function(response) {
                    var playerStatePrivate = response.data.playerStatePrivate
                    app.name = playerStatePrivate.name
                    app.money = playerStatePrivate.money
                    app.totalMoneyInPot = playerStatePrivate.totalMoneyInPot
                    app.cardFileUris = playerStatePrivate.cardFileUris
                    app.communityCardUris = playerStatePrivate.communityCardUris
                    app.gameStateSequenceNumber = playerStatePrivate.gameStateSequenceNumber
                    app.stateChangeMessages = playerStatePrivate.stateChangeMessages
                    app.whoHasTheTurn = playerStatePrivate.whoHasTheTurn
                    app.dealer = playerStatePrivate.dealer;
                    app.isOnTurn = playerStatePrivate.isOnTurn
                    app.isAllIn = playerStatePrivate.isAllIn
                    app.isDealer = playerStatePrivate.isDealer
                    app.hasFolded = playerStatePrivate.hasFolded
                    app.players = response.data.playerStatesPublic
                    positionPlayersAroundTable(app.players)
                    longPoll();
                })
        })
        .catch(function(error) {
            numConsecutiveErrors++
            if (numConsecutiveErrors >= 5) {
                app.message = 'Got ' + numConsecutiveErrors + ' errors connecting to server. Refresh page to try again'
                return
            }
            var retrySecs = numConsecutiveErrors * 1000
            app.message = 'Got ' + numConsecutiveErrors +
                ' errors connecting to server. Automatically retrying in ' +
                 numConsecutiveErrors + ' seconds.'
            window.setTimeout(function() { longPoll() }, retrySecs)
        })
}

function setPlayerSecretToken(playerSecretToken) {
    var expiry = new Date()
    expiry.setFullYear(expiry.getFullYear() + 1)
    document.cookie = "player-secret-token=" + playerSecretToken + "; expires=" + expiry.toUTCString() + ";path=/"
}

// Given the player objects as an array, this method calculates coordinates to place
// the players evenly in an ellipse around the (somewhat elliptical) card-table. The
// positions are added as a property on the player object and set in the HTML on the
// style attribute as the 'left' and 'top' properties to position the 'div' representing
// each player
function positionPlayersAroundTable(players) {
    // formula for ellipse in polar coordinates is:
    //
    // (x, y) = (a cos t, b sin t)
    //
    // a is the x-radius of the ellipse
    // b is the y-radius of the ellipse
    //
    // Source: https://en.wikipedia.org/wiki/Ellipse#Parametric_representation
    //
    // Given n players, we want to distribute them with evenly distributed angular
    // separation between them. So given 2*PI degrees of real estate, the i-th player
    // will be located at angle: (2*PI/n) * i
    var a = 400 // matches the style="width:a" attribute of the div id="poker-table"
    var b = 250 // matches the style="height:a" attribute of the div id="poker-table"
    var playerBoxSizeX = 170 // should match the .player-box css style
    var playerBoxSizeY = 140
    var betInfoDivSizeX = 27
    var betInfoDivSizeY = 23
    var betInfoDistanceToCenterRatio = 1/2
    var numberOfPlayers = players.length
    var angularSeparation = 2 * Math.PI / numberOfPlayers
    for (var i = 0; i < numberOfPlayers; i++) {
        var theta = angularSeparation * i
        var x = a * Math.cos(theta)
        var y = b * Math.sin(theta)
        var xBet = betInfoDistanceToCenterRatio * x
        var yBet = betInfoDistanceToCenterRatio * y
        x += a - playerBoxSizeX/2
        y += b - playerBoxSizeY/2
        xBet += a - betInfoDivSizeX/2
        yBet += b - betInfoDivSizeY/2
        x = Math.trunc(x) + 'px'
        y = Math.trunc(y) + 'px'
        xBet = Math.trunc(xBet) + 'px'
        yBet = Math.trunc(yBet) + 'px'
        var player = players[i]
        player.xy = [x, y]
        player.xyBet = [xBet, yBet]
        player.playerIdx = i
        player.cssClass = 'player-box'
        if (player.hasFolded) {
            player.cssClass += ' folded-player-box'
        } else if (player.isOnTurn) {
            player.cssClass += ' turn-player-box'
        }
        if (player.isAllIn) {
            player.cssClass += ' allin-player-box'
        }
    }
}

longPoll()