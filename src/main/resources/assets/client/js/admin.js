var app = new Vue({
  el: '#app',
  data: {
    message: undefined,
    adminSecretToken: undefined,
    isLoggedInAsAdmin: false,
    playerNames: []
  },

  methods: {
    listPlayers: function() {
        if (!this.adminSecretToken) {
            this.message = 'Paste in your admin secret token'
            return
        }
        var expiry = new Date()
        expiry.setFullYear(expiry.getFullYear() + 1)
        var cookieHdr = "admin-secret-token=" + this.adminSecretToken + "; expires=" + expiry.toUTCString() + ";path=/"
        document.cookie = cookieHdr

        var that = this
        axios.get('/api/admin/listplayers')
            .then(function(response) {
                that.isLoggedInAsAdmin = true
                that.playerNames = response.data.playerNames
            })
            .catch(function(error) {
                that.message = 'Error connecting to server, try again'
            })
    },

    startGame: function() {
        var that = this
        axios.post('/api/admin/startgame', {})
            .then(function(response) {
                that.message = 'Game started'
            })
            .catch(function(error) {
                that.message = 'Error connecting to server, try again'
            })
    }
  }
})

if (document.cookie.startsWith("admin-secret-token=")) {
    app.adminSecretToken = document.cookie.slice(document.cookie.indexOf('=') + 1)
    app.message = "Admin secret token already stored in browser"
}