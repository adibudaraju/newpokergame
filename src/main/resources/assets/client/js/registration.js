var app = new Vue({
  el: '#app',
  data: {
    playerName: undefined,
    message: undefined,
    playerSecretToken: undefined
  },
  methods: {
    register: function() {
        var that = this
        axios.post('/api/player/register', { name: this.playerName })
            .then(function(response) {
                var respData = response.data
                if (respData.success) {
                    that.message = 'Registration successful'
                    that.playerSecretToken = respData.playerSecretToken
                    var expiry = new Date()
                    expiry.setFullYear(expiry.getFullYear() + 1)
                    document.cookie = "player-secret-token=" + respData.playerSecretToken + "; expires=" + expiry.toUTCString() + ";path=/"
                } else {
                    that.message = respData.error
                }
            })
            .catch(function(error) {
                that.message = 'Server failed, refresh page and try again'
            })
      }
  }
})