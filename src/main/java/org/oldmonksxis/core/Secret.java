package org.oldmonksxis.core;

import java.security.SecureRandom;
import java.util.Base64;

public class Secret {

    private static SecureRandom random = new SecureRandom();

    public static String generateSecretToken(int length) {
        byte[] randomBytes = new byte[128];
        random.nextBytes(randomBytes);
        return Base64.getEncoder().encodeToString(randomBytes);
    }
}
