package org.oldmonksxis.gameengine;

import org.oldmonksxis.gameengine.errors.CommandRejectedException;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static org.oldmonksxis.core.Secret.generateSecretToken;

public class Registration {
    private boolean gameStarted;
    private HashMap<String, PlayerState> playerRegistrations = new HashMap<>();
    private final String avatarFilesRootUri = "/assets/client/png/avatars/";
    private String[] avatarFiles = new String[] {
        "starwars-chewbacca.png",
        "starwars-darth_maul.png",
        "starwars-princess_amidala.png",
        "starwars-royal_guard.png",
        "starwars-the_emperor.png",
        "starwars-yoda.png"
    };
    private int nextCookieForFakeGame;
    private int nextAvatarIdx = new Random().nextInt(avatarFiles.length);
    private boolean isFakeGame;

    public Registration(boolean isFakeGame) {
        this.isFakeGame = isFakeGame;
    }

    public synchronized String playerRegister(String playerName) {
        if (gameStarted) throw new CommandRejectedException("Game has already started");
        if (playerName.length() == 0
            || !Character.isLetter(playerName.chars().findFirst().getAsInt())
            || playerName.chars().anyMatch(ch -> !(Character.isLetterOrDigit(ch) || (ch == ' ')))) {

            throw new CommandRejectedException("Invalid player name, can only include alphabets, numbers or space and must start with an alphabet");
        }
        String secretToken = generateSecretToken(32);
        nextAvatarIdx = (nextAvatarIdx + 1) % avatarFiles.length;
        String nextiAvatarUri = avatarFilesRootUri + avatarFiles[nextAvatarIdx];
        PlayerState playerState = new PlayerState(100, playerName, secretToken, nextiAvatarUri);
        playerRegistrations.put(secretToken, playerState);
        return secretToken;
    }

    public synchronized String getNextCookieForFakeGame() {
        if (nextCookieForFakeGame >= playerRegistrations.size()) {
           throw new RuntimeException(
                   MessageFormat.format(
                           "Max of {0} players already registered",
                           playerRegistrations.size()));
        }
        List<String> sortedPlayerNames = playerRegistrations.keySet().stream().sorted().collect(Collectors.toList());
        return sortedPlayerNames.get(nextCookieForFakeGame++);
    }

    public synchronized List<String> listPlayerNames() {
        return playerRegistrations.values().stream().map(p -> p.getName()).collect(Collectors.toList());
    }

    public synchronized BettingRound adminStartGame() {
        gameStarted = true;
        return new BettingRound(
                playerRegistrations.values().stream().collect(Collectors.toList()),
                100,
                isFakeGame);
    }
}
