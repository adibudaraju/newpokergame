package org.oldmonksxis.gameengine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

public class PlayerState {
    static final Logger logger = LoggerFactory.getLogger(PlayerState.class);
    private List<Integer> playerCards;
    private String name;
    private String secretoken;
    private int money;
    private int moneyInPot;
    private int totalContributionToPot;
    private Hand bestHandSoFar;
    private boolean hasFolded;
    private boolean isAllIn;
    private boolean cardsShown;
    private String avatarUri;

    public PlayerState(int money, String name, String secretoken,
                       String avatarUri) {
        this.money = money;
        this.moneyInPot = 0;
        this.totalContributionToPot = 0;
        this.isAllIn = false;
        this.name = name;
        this.secretoken = secretoken;
        this.avatarUri = avatarUri;
    }

    public void goAllIn() {
        isAllIn = true;
    }

    public boolean isAllIn() {
        return isAllIn;
    }

    public void resetAllIn() {
        isAllIn = false;
    }

    public String getName() {
        return name;
    }

    public String getSecretoken() {
        return secretoken;
    }

    public int getMoney() {
        return money;
    }

    public Hand getBestHandSoFar() {
        return bestHandSoFar;
    }

    public void unfold() {
        hasFolded = false;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void bet(int amount) {
        this.money -= amount;
        this.moneyInPot += amount;
        this.totalContributionToPot += amount;
    }

    public void add(int amount) {
        this.money += amount;
    }

    public void setBestHandSoFar(Hand h) {
        bestHandSoFar = h;
    }

    public void dealCards(List<Integer> playerCards) {
        this.playerCards = playerCards;
    }

    public List<Integer> getPlayerCards() {
        return playerCards;
    }

    public void fold() {
        hasFolded = true;
    }

    public void resetMoneyInPot() {
        moneyInPot = 0;
    }

    public int getTotalContributionToPot() {
        return totalContributionToPot;
    }

    public void resetTotalContributionToPot() {
        totalContributionToPot = 0;
    }

    public void show() {
        cardsShown = true;
    }

    public int getMoneyInPot() {
        return this.moneyInPot;
    }

    public boolean cardsShown() {
        return cardsShown;
    }

    public boolean hasFolded() {
        return hasFolded;
    }

    public void dump(List<String> orderedCardList) {
        logger.info("Cards: {}", playerCards.stream().map(n -> orderedCardList.get(n)).collect(Collectors.toList()));
        logger.info("Money: {}", money);
    }


    public String getAvatarUri() {
        return avatarUri;
    }
}
