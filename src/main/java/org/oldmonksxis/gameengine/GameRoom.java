package org.oldmonksxis.gameengine;

public class GameRoom {
    private Registration registration;
    private BettingRound bettingRound;

    public GameRoom(boolean isFakeGame) {
        this.registration = new Registration(isFakeGame);
    }

    public void startGame() {
        bettingRound = this.registration.adminStartGame();
    }

    public void setBettingRound(BettingRound bettingRound) {
        this.bettingRound = bettingRound;
    }

    public Registration getRegistration() {
        return registration;
    }

    public BettingRound getBettingRound() {
        return bettingRound;
    }
}
