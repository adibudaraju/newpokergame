package org.oldmonksxis.gameengine;

import org.oldmonksxis.api.PlayerStateResponse;
import org.oldmonksxis.api.PlayerStateResponse.PlayerStatePrivate;
import org.oldmonksxis.api.PlayerStateResponse.PlayerStatePublic;
import org.oldmonksxis.gameengine.errors.CommandRejectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

public class BettingRound {
    static final Logger logger = LoggerFactory.getLogger(BettingRound.class);
    static final String CardImageDir = Paths.get(Paths.get(System.getProperty("user.dir")).toString(),
            "src", "main", "resources", "assets", "client", "png", "cards").toString();
    static final String CardImageUriPrefix = "/assets/client/png/cards/";
    public static final List<String> CardImageFileNamesInOrder;
    public static final List<String> CardImageRelativeUrisInOrder;

    private LinkedList<String> stateChangeMessages = new LinkedList<>();

    enum BettingPhase {
        /**
         * After the cards are dealt, before the 3 cards are flopped
         */
        DealCompleted,

        /**
         * After the 3 cards are flopped
         */
        FlopCompleted,

        /**
         * After the 4th card is turned, before the final card
         */
        TurnCompleted,

        /**
         * After the final card is shown
         */
        RiverCompleted
    }

    private List<Integer> shuffledGameDeck;
    private List<PlayerState> playerStates;
    private boolean isFakeGame;
    private List<Integer> communityCards;
    private int gameStateCounter;
    private String message;
    private int dealerPlayerIdx;
    private int bigBlindIdx;
    private int smallBlindIdx;
    private int playerOnTurnIdx;
    private int mostRecentRaiseIdx; //mostRecentRaiseIdx is whoever raised last
    private int moneyInPot;
    private int bigBlindAmount;
    private int smallBlindAmount;
    private int currentPotMinimumBet;
    private BettingPhase bettingPhase;
    private Semaphore waiter = new Semaphore(0);

    static {
        File cardImageDir = new File(CardImageDir);
        CardImageFileNamesInOrder = Arrays.stream(cardImageDir.listFiles())
                .map(f -> f.getName())
                .filter(n ->
                {
                    // Use the 2nd image for the picture cards, they're prettier
                    if (n.matches("(jack|queen|king)_.+2.png"))
                        return true;
                    if (n.matches("\\d+_.+"))
                        return true;

                    // Use the 2nd image of ace-of-spades, prettier
                    if (n.startsWith("ace")) {
                        if (n.contains("spades")) {
                            return n.endsWith("2.png");
                        }
                        return true;
                    }
                    return false;
                })
                .collect(Collectors.toList());

        CardImageRelativeUrisInOrder = CardImageFileNamesInOrder.stream()
                .map(f -> CardImageUriPrefix + f)
                .collect(Collectors.toList());
    }

    public BettingRound(List<PlayerState> playerStates, int moneyPerPlayer, boolean isFakeGame) {
        this.playerStates = playerStates;
        this.isFakeGame = isFakeGame;

        gameStateCounter = 1;

        bigBlindAmount = 2;
        smallBlindAmount = 1;

        stateChangeMessages.addFirst("Game started");

        dealerPlayerIdx = -1;
        smallBlindIdx = 0;
        bigBlindIdx = 1;

        newRound();
    }

    /**
     * every time after the previous round ends, a new round starts - a lot of the fields will reset
     * as well
     */
    private void newRound() {
        shuffledGameDeck = new ArrayList<>();
        for (int i = 0; i < CardImageRelativeUrisInOrder.size(); i++) {
            shuffledGameDeck.add(i);
        }
        // Shuffle, then deal cards to players
        Collections.shuffle(shuffledGameDeck);
        for (PlayerState playerState : playerStates) {
            Integer card1 = shuffledGameDeck.remove(0);
            Integer card2 = shuffledGameDeck.remove(0);
            List<Integer> cardsForPlayer = new ArrayList<>();
            cardsForPlayer.add(card1);
            cardsForPlayer.add(card2);
            playerState.dealCards(cardsForPlayer);
            playerState.setBestHandSoFar(getHand(getFileNamesFromInts(playerState.getPlayerCards())));
            playerState.unfold();
            playerState.resetAllIn();
            playerState.resetMoneyInPot();
            playerState.resetTotalContributionToPot();
        }

        bettingPhase = BettingPhase.DealCompleted;
        communityCards = new ArrayList<>();
        moneyInPot = 0;

        //rotating each new round
        dealerPlayerIdx = getNextPlayerIdx(dealerPlayerIdx);
        smallBlindIdx = getNextPlayerIdx(smallBlindIdx);
        bigBlindIdx = getNextPlayerIdx(bigBlindIdx);

        //informing players
        logger.info("New hand started with {} having the dealer button", dealerPlayerIdx);
        notifyGameStateChanged(MessageFormat.format("New hand started, {0} has the dealer button",
                playerStates.get(dealerPlayerIdx).getName()));

        // preliminary bet logic
        playerStates.get(smallBlindIdx).bet(smallBlindAmount);
        moneyInPot += smallBlindAmount;
        playerStates.get(bigBlindIdx).bet(bigBlindAmount);
        moneyInPot += bigBlindAmount;
        logger.info("Player {} has put the small blind of ${} and has ${} remaining. Pot now has ${}",
                smallBlindIdx, smallBlindAmount, playerStates.get(smallBlindIdx).getMoney(),
                moneyInPot - bigBlindAmount);
        notifyGameStateChanged(MessageFormat.format("{0} bet ${1} as the small blind",
                playerStates.get(smallBlindIdx).getName(), smallBlindAmount));
        logger.info("Player {} has put the big blind of ${} and has ${} remaining. Pot now has ${}",
                bigBlindIdx, bigBlindAmount, playerStates.get(bigBlindIdx).getMoney(),
                moneyInPot);
        notifyGameStateChanged(MessageFormat.format("{0} bet ${1} as the big blind",
                playerStates.get(bigBlindIdx).getName(), bigBlindAmount));


        playerOnTurnIdx = getNextPlayerIdx(bigBlindIdx);
        mostRecentRaiseIdx = playerOnTurnIdx;
        currentPotMinimumBet = bigBlindAmount;

        logger.info("Turn on {}", playerOnTurnIdx);
    }

    /**
     * helper method to modularly increment
     */
    private int getNextPlayerIdx(int idx) {
        return (idx + 1) % playerStates.size();
    }

    public synchronized int getPlayerIndex(String playerSecretToken) {
        for (int i = 0; i < playerStates.size(); i++) {
            if (playerStates.get(i).getSecretoken().equals(playerSecretToken)) {
                return i;
            }
        }
        throw new CommandRejectedException("Player not found");
    }

    public synchronized List<PlayerState> getPlayerStates() {
        return playerStates;
    }

    public synchronized List<Integer> getCommunityCards() {
        return communityCards;
    }

    public synchronized List<String> getCommunityCardFileNames() {
        return communityCards.stream().map(n -> CardImageFileNamesInOrder.get(n)).collect(Collectors.toList());
    }

    public synchronized List<String> getCommunityCardFileNameUris() {
        return communityCards.stream().map(n -> CardImageRelativeUrisInOrder.get(n)).collect(Collectors.toList());
    }

    public synchronized void flop() {
        if (bettingPhase != BettingPhase.DealCompleted)
            throw new CommandRejectedException("You can only flop after the deal");
        communityCards.add(shuffledGameDeck.remove(0));
        communityCards.add(shuffledGameDeck.remove(0));
        communityCards.add(shuffledGameDeck.remove(0));
        List<String> communityCardNamesDebug = getCommunityCardFileNames();
        logger.info("Cards turned over in flop: {}", communityCardNamesDebug);
        notifyGameStateChanged(MessageFormat.format("{0} showed the flop", playerStates.get(dealerPlayerIdx).getName()));
        bettingPhase = BettingPhase.FlopCompleted;
    }

    public synchronized void show(int playerIdx) {
        PlayerState player = playerStates.get(playerIdx);
        if (!player.hasFolded())
            throw new CommandRejectedException("You can only show cards after you have folded");
        if (player.cardsShown())
            throw new CommandRejectedException("You have already shown your cards");
        String cards = player.getPlayerCards().stream().map(n -> parseCardFullNameFromFileString(CardImageFileNamesInOrder.get(n)))
                .collect((Collectors.joining(", ")));
        logger.info("Cards shown: {}", cards);
        notifyGameStateChanged(MessageFormat.format("{0} showed his cards: " + cards, player.getName()));
        player.show();

    }

    private List<String> getFileNamesFromInts(List<Integer> nums) {
        List<String> names = new ArrayList<String>();
        for (int i : nums)
            names.add(CardImageFileNamesInOrder.get(i));
        return names;
    }

    private String parseCardFullNameFromFileString(String fileString) {
        if (!fileString.endsWith(".png"))
            return "Unknown Card";

        String[] pieces = fileString.split("_");
        String val = pieces[0].toUpperCase();
        if (!val.equals("10"))
            val = val.substring(0, 1);
        char suit = pieces[2].toLowerCase().charAt(0);
        String symb = "";
        switch (suit) {
            case 's':
                symb = "\u2660";
                break; // unicode value for spades
            case 'c':
                symb = "\u2663";
                break; // unicode value for clubs
            case 'h':
                symb = "\u2665";
                break; // unicode value for hearts
            case 'd':
                symb = "\u2666";
                break; // unicode value for diamonds
            default:
                return "Card suit unidentifiable";
        }
        return val + symb;

    }

    private int parseCardSuitFromFileString(String fileString) {
        char c = fileString.split("_")[2].toLowerCase().charAt(0);
        switch (c) {
            case 'c':
                return 0;
            case 's':
                return 1;
            case 'd':
                return 2;
            default:
                return 3;
        }
    }

    private int parseCardValFromFileString(String fileString) {
        String[] pieces = fileString.split("_");
        String val = pieces[0].toLowerCase();
        try {
            return Integer.parseInt(val);
        } catch (NumberFormatException e) {
            switch (val.charAt(0)) {
                case 'j':
                    return 11;
                case 'q':
                    return 12;
                case 'k':
                    return 13;
                case 'a':
                    return 14;
                default:
                    return -1;

            }
        }
    }

    public synchronized void turn() {
        if (bettingPhase != BettingPhase.FlopCompleted)
            throw new CommandRejectedException("You can only turn after the flop is done");
        communityCards.add(shuffledGameDeck.remove(0));
        List<String> communityCardNamesDebug = getCommunityCardFileNames();
        logger.info("Cards turned over in turn: {}", communityCardNamesDebug);
        notifyGameStateChanged(MessageFormat.format("{0} showed the turn card", playerStates.get(dealerPlayerIdx).getName()));
        bettingPhase = BettingPhase.TurnCompleted;
    }

    public synchronized void river() {
        if (bettingPhase != BettingPhase.TurnCompleted)
            throw new CommandRejectedException("You can only show the river card after the turn");
        communityCards.add(shuffledGameDeck.remove(0));
        List<String> communityCardNamesDebug = getCommunityCardFileNames();
        logger.info("Cards turned over in river: {}", communityCardNamesDebug);
        notifyGameStateChanged(MessageFormat.format("{0} showed the river card", playerStates.get(dealerPlayerIdx).getName()));
        bettingPhase = BettingPhase.RiverCompleted;
    }

    public synchronized void playerPlaceBet(int playerIdx, int moneyAmount) {
        //NOTE: "Betting", as of now, means ADDING to your current contribution, NOT replacing it.
        PlayerState playerState = playerStates.get(playerIdx);

        if (playerIdx != playerOnTurnIdx)
            throw new CommandRejectedException("Player " + playerIdx + " may not place bet now. Turn is on " + playerOnTurnIdx);

        if (playerState.getMoneyInPot() + moneyAmount < currentPotMinimumBet && playerState.getMoney() > moneyAmount)
            throw new CommandRejectedException("Bet of " + moneyAmount + " rejected. " +
                    "Must bet at least " + (currentPotMinimumBet - playerState.getMoneyInPot() + " in order to call."));
        if (playerState.getMoney() < moneyAmount)
            throw new CommandRejectedException("Player " + playerIdx + " has only " + playerState.getMoney() + " and may not bet " + moneyAmount);


        playerState.bet(moneyAmount);
        moneyInPot += moneyAmount;

        boolean betLogged = false;
        if (playerState.getMoney() == 0) {
            playerState.goAllIn();
            logger.info("Player {} went all in by betting {} and has {} remaining. Pot now has {}", playerIdx,
                    moneyAmount, playerState.getMoney(), moneyInPot);
            notifyGameStateChanged(MessageFormat.format("{0} went all in at ${1}",
                    playerStates.get(playerOnTurnIdx).getName(), moneyAmount));
            betLogged = true;
        }
        if (playerState.getMoneyInPot() == currentPotMinimumBet && !betLogged) {
            logger.info("Player {} called  at {} and has {} remaining. Pot now has {}", playerIdx,
                    currentPotMinimumBet, playerState.getMoney(), moneyInPot);
            notifyGameStateChanged(MessageFormat.format("{0} called at ${1}",
                    playerStates.get(playerOnTurnIdx).getName(), currentPotMinimumBet));
        }
        if (playerState.getMoneyInPot() > currentPotMinimumBet) {
            mostRecentRaiseIdx = playerIdx;
            currentPotMinimumBet = playerState.getMoneyInPot();
            if (!betLogged) {
                logger.info("Player {} raised to {} and has {} remaining. Pot now has {}", playerIdx,
                        currentPotMinimumBet, playerState.getMoney(), moneyInPot);
                notifyGameStateChanged(MessageFormat.format("{0} raised to ${1}",
                        playerStates.get(playerOnTurnIdx).getName(), currentPotMinimumBet));
            }

        }

        nextPlayersTurn(); // auto-transitions


    }

    public synchronized void fold(int playerIdx) {
        if (playerIdx != playerOnTurnIdx)
            throw new CommandRejectedException("Player " + playerIdx + " may not fold now. Turn is on " + playerOnTurnIdx);
        PlayerState playerState = playerStates.get(playerIdx);
        playerState.fold();
        logger.info("{} folded", playerIdx);
        notifyGameStateChanged(MessageFormat.format("{0} folded", playerStates.get(playerOnTurnIdx).getName()));
        nextPlayersTurn();
    }


    public synchronized List<String> getPlayerCards(int playerNum) {
        return playerStates.get(playerNum)
                .getPlayerCards()
                .stream()
                .map(cardIdx -> CardImageRelativeUrisInOrder.get(cardIdx))
                .collect(Collectors.toList());
    }

    public synchronized PlayerStateResponse getPlayerStateResponse(int playerIdx) {
        ArrayList<PlayerStatePublic> playerStatesPublic = new ArrayList<>();
        int idx = 0;
        for (PlayerState playerState : playerStates) {
            PlayerStatePublic playerStatePublic = new PlayerStatePublic(
                    playerState.getName(),
                    playerState.getMoney(),
                    playerState.getMoneyInPot(),
                    moneyInPot,
                    playerState.hasFolded(),
                    idx == playerOnTurnIdx,
                    idx == dealerPlayerIdx,
                    playerState.isAllIn(),
                    playerState.getAvatarUri(),
                    isFakeGame ? playerState.getSecretoken() : null);
            playerStatesPublic.add(playerStatePublic);
            idx++;
        }

        PlayerState playerState = playerStates.get(playerIdx);

        List<String> playerCardUris = playerState.getPlayerCards().stream()
                .map(i -> CardImageRelativeUrisInOrder.get(i))
                .collect(Collectors.toList());

        List<String> communityCardUris = communityCards.stream()
                .map(i -> CardImageRelativeUrisInOrder.get(i))
                .collect(Collectors.toList());

        List<String> lastFewMessages = stateChangeMessages.stream().limit(20).collect(Collectors.toList());

        PlayerStatePrivate playerStatePrivate = new PlayerStatePrivate(
                playerState.getName(),
                playerState.getMoney(),
                playerState.getMoneyInPot(),
                moneyInPot,
                playerState.getBestHandSoFar(),
                playerCardUris,
                communityCardUris,
                gameStateCounter,
                lastFewMessages,
                playerStates.get(playerOnTurnIdx).getName(),
                playerStates.get(dealerPlayerIdx).getName(),
                playerIdx == playerOnTurnIdx,
                playerIdx == dealerPlayerIdx,
                playerState.isAllIn(),
                playerState.hasFolded());

        return new PlayerStateResponse(playerStatePrivate, playerStatesPublic);
    }

    public synchronized void adminEditPot(int money) {
        logger.info("Admin action: Changing pot from {} -> {}", moneyInPot, money);
        notifyGameStateChanged(MessageFormat.format("The admin adjusted the pot from {0} to {1}", moneyInPot, money));
        this.moneyInPot = money;
    }

    public synchronized void adminEditPlayerMoney(int playerIdx, int money) {
        PlayerState playerState = this.playerStates.get(playerIdx);
        logger.info("Admin action: Player {} changing money from {} -> {}", playerIdx, playerState.getMoney(), money);
        notifyGameStateChanged(MessageFormat.format("The admin adjusted {0}'s money from {1} to {2}",
                playerStates.get(playerOnTurnIdx).getName(), playerState.getMoney(), money));
        playerState.setMoney(money);
    }

    private synchronized void notifyGameStateChanged(String lastStateChange) {
        this.stateChangeMessages.addFirst(lastStateChange);
        gameStateCounter++;
        this.notifyAll();
    }

    public synchronized int waitForGameStateChange(int clientGameStateCounter) {
        while (true) {
            if (clientGameStateCounter < gameStateCounter) {
                return gameStateCounter;
            }

            try {
                this.wait(30_000);
                return gameStateCounter;
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public synchronized int getGameStateCounter() {
        return gameStateCounter;
    }

    private void nextPlayersTurn() {
        List<PlayerState> unfoldedPlayers = playerStates.stream()
                .filter(p -> !p.hasFolded())
                .collect(Collectors.toList());

        if (unfoldedPlayers.size() == 1) {
            logger.info("Player {} won the hand as the last person not folded",
                    playerStates.indexOf(unfoldedPlayers.get(0)));
            notifyGameStateChanged(MessageFormat.format("{0} won the hand, as everybody else folded",
                    unfoldedPlayers.get(0).getName()));
            List<Integer> winner = new ArrayList<Integer>();
            winner.add(playerStates.indexOf(unfoldedPlayers.get(0)));
            declareWinner(winner);
            return;
        }

        List<PlayerState> activePlayers = playerStates.stream()
                .filter(p -> !p.hasFolded() && !p.isAllIn())
                .collect(Collectors.toList());

        if (activePlayers.size() == 0) {
            finishPhase();
            return;
        }

        playerOnTurnIdx = getNextPlayerIdx(playerOnTurnIdx);
        while (playerStates.get(playerOnTurnIdx).hasFolded() || playerStates.get(playerOnTurnIdx).isAllIn())//as long as they havent folded
        {
            if (playerOnTurnIdx == mostRecentRaiseIdx && playerStates.get(playerOnTurnIdx).isAllIn()) {
                finishPhase();
                return;
            }
            playerOnTurnIdx = getNextPlayerIdx(playerOnTurnIdx);
        }

        // no one has raised in the past cycle
        if (playerOnTurnIdx == mostRecentRaiseIdx) {
            finishPhase();
            return;
        }


        logger.info("Moved to turn of player {}", playerOnTurnIdx);
        notifyGameStateChanged(MessageFormat.format("Turn moved to {0}",
                playerStates.get(playerOnTurnIdx).getName()));
    }


    private void finishPhase() {
        switch (bettingPhase) {
            case DealCompleted:
                flop();
                break;
            case FlopCompleted:
                turn();
                break;
            case TurnCompleted:
                river();
                break;
            case RiverCompleted:
                computeWinner();
                return;
            default:
                throw new CommandRejectedException("Phase of game not recognized");
        }
        for (PlayerState ps : playerStates) {
            ps.resetMoneyInPot();
        }
        playerOnTurnIdx = (smallBlindIdx - 1) % playerStates.size();
        mostRecentRaiseIdx = -1;
        currentPotMinimumBet = -1;
        updatePlayersBestHands();
        nextPlayersTurn();
        return;
    }

    private void updatePlayersBestHands() {
        for (int i = 0; i < playerStates.size(); i++) {
            playerStates.get(i).setBestHandSoFar(getHand(getAllCards(i)));
        }
    }

    private List<String> getAllCards(int playerIdx) {
        List<String> cards = getCommunityCardFileNames();
        cards.addAll(getFileNamesFromInts(playerStates.get(playerIdx).getPlayerCards()));
        return cards;
    }

    public void computeWinner() {
        List<PlayerState> states = sortPlayersByContributions();
        PlayerState first;
        int lowestAmt = 0, prevLowestAmt, potNum = 0, additionalMoney = 0;
        while (!states.isEmpty()) {
            first = states.get(0);
            if (first.hasFolded()) {
                additionalMoney += first.getTotalContributionToPot();
                states.remove(0);
                continue;
            }
            potNum++;
            prevLowestAmt = lowestAmt;
            lowestAmt = first.getTotalContributionToPot();
            computeWinnerOfSidePot(states, (lowestAmt - prevLowestAmt) * states.size() + additionalMoney, potNum);
            deleteLowestContributions(states);
            additionalMoney = 0;
        }
        newRound();

    }

    private void deleteLowestContributions(List<PlayerState> states) {
        if (states == null)
            return;
        int low = states.get(0).getTotalContributionToPot();
        while (states.size() > 0 && states.get(0).getTotalContributionToPot() == low)
            states.remove(0);
    }

    private List<PlayerState> sortPlayersByContributions() {
        PlayerState temp;
        boolean sorted = false;
        List<PlayerState> states = (List<PlayerState>) (((ArrayList<PlayerState>) (playerStates)).clone());
        while (!sorted) {
            sorted = true;
            for (int i = 0; i < states.size() - 1; i++) {
                if (states.get(i).getTotalContributionToPot() - states.get(i + 1).getTotalContributionToPot() > 0) {
                    temp = states.get(i);
                    states.set(i, states.get(i + 1));
                    states.set(i + 1, temp);
                    sorted = false;
                }
            }
        }
        return states;
    }

    private void computeWinnerOfSidePot(List<PlayerState> players, int amt, int potNum) {
        int temp;
        List<Integer> winners = new ArrayList<Integer>();
        if (players == null)
            return;
        winners.add(0);

        for (int i = 1; i < players.size(); i++) {
            if (players.get(i).hasFolded())
                continue;
            temp = players.get(i).getBestHandSoFar().compareTo(
                    players.get(winners.get(0)).getBestHandSoFar());
            if (temp > 0) {
                winners = new ArrayList<Integer>();
                winners.add(i);
            } else if (temp == 0) {
                winners.add(i);
            }

        }
        List<PlayerState> winPlayers = new ArrayList<PlayerState>();
        for(int i: winners)
            winPlayers.add(players.get(i));
        StringBuilder log = new StringBuilder();
        StringBuilder mess = new StringBuilder();
        mess.append(winPlayers.get(0).getName());
        log.append(playerStates.indexOf(winPlayers.get(0)));
        for (int i = 1; i < winners.size(); i++) {
            log.append(", ");
            log.append(playerStates.indexOf(winPlayers.get(i)));
            mess.append(",");
            mess.append(winPlayers.get(i).getName());
        }
        logger.info("{} won pot #{}, earning ${} (each) - winning hand: {}", log.toString(), potNum, amt / winners.size(),
                players.get(winners.get(0)).getBestHandSoFar().toString());
        notifyGameStateChanged(MessageFormat.format("{0} won pot #{1} with a {2}, winning ${3} (each)",
                mess.toString(), potNum, winPlayers.get(0).getBestHandSoFar().toString(),
                amt / winners.size()));
        declareWinner(winPlayers, amt, potNum);
    }

    public synchronized void declareWinner(List<Integer> playerNums) {
        int sharedAmt = moneyInPot / playerNums.size();
        for (int playerNum : playerNums)
            playerStates.get(playerNum).add(sharedAmt);
        newRound();
    }

    public synchronized void declareWinner(List<PlayerState> players, int amt, int potNum) {
        int sharedAmt = amt / players.size();
        for (PlayerState winner: players)
            winner.add(sharedAmt);
    }

    public Hand getHand(List<String> filenames) {
        List<Integer> vals, suits;
        vals = new ArrayList<Integer>();
        suits = new ArrayList<Integer>();
        for (String s : filenames) {
            vals.add(parseCardValFromFileString(s));
            suits.add(parseCardSuitFromFileString(s));
        }
        return new Hand(vals, suits);
    }

    public int isLargerHand(List<String> hand1, List<String> hand2) {
        return getHand(hand1).compareTo(getHand(hand2));
    }


    public int getDealerPlayerIdx() {
        return dealerPlayerIdx;
    }

    public int getPlayerOnTurnIdx() {
        return playerOnTurnIdx;
    }

    public int getMoneyInPot() {
        return moneyInPot;
    }

    // Legacy code was used for testing
    public void blockForChange() {
        try {
            waiter.acquire();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public String getLatestMessage() {
        return message;
    }

    public void setLatestMessage(String message) {
        this.message = message;
        waiter.release();
    }

    public void shellDebugContributions() {
        StringBuilder sb = new StringBuilder();
        for (PlayerState ps : playerStates) {
            sb.append(ps.getMoneyInPot());
            sb.append(" ");
        }

        logger.info("Contributions: " + sb.toString());
    }

    public void shellDebugTotals() {
        StringBuilder sb = new StringBuilder();
        for (PlayerState ps : playerStates) {
            sb.append(ps.getMoneyInPot());
            sb.append(" ");
        }

        logger.info("Contributions: " + sb.toString());
    }

    public void shellDebugMoneys() {
        StringBuilder sb = new StringBuilder();
        for (PlayerState p : playerStates) {
            sb.append(p.getMoney());
            sb.append(" ");
        }
        logger.info("Moneys: " + sb.toString());

    }
}
