package org.oldmonksxis.gameengine;

import java.util.*;
import java.util.stream.Collectors;

public class Hand implements Comparable<Hand> {

    public static final String[] handTypes = new String[]{"High Card", "Pair", "Two Pair", "Three" +
            " of a Kind, Straight, Flush, Full House, Four of a Kind, Straight Flush"};
    private List<Integer> cardVals;
    private List<Integer> cardSuits;
    private Map<Integer, Integer> cardDensity;
    private int handType;
    private int size;
    private int handValue;

    public Hand(List<Integer> vals, List<Integer> suits) {
        cardVals = vals;
        cardSuits = suits;
        size = cardVals.size();
        while (cardVals.size() < 7) {
            cardVals.add(0);
            cardSuits.add(0);
        }
        sortVals();
        generateDensity();
        computeHand();
    }

    private void set(int type, int value) {
        handType = type;
        handValue = value;
    }

    private void computeHand() {
        int temp;
        temp = getStraightFlush();
        if (temp != -1) {
            set(8, temp);
            return;
        }
        temp = get4OfKind();
        if (temp != -1) {
            set(7, temp);
            return;
        }
        temp = getFullHouse();
        if (temp != -1) {
            set(6, temp);
            return;
        }
        temp = getFlush();
        if (temp != -1) {
            set(5, temp);
            return;
        }
        temp = getStraight();
        if (temp != -1) {
            set(4, temp);
            return;
        }
        temp = get3OfKind();
        if (temp != -1) {
            set(3, temp);
            return;
        }
        temp = get2Pair();
        if (temp != -1) {
            set(2, temp);
            return;
        }
        temp = getPair();
        if (temp != -1) {
            set(1, temp);
            return;
        }
        set(0, getHighCard());

    }

    public int compareTo(Hand other) {
        if (handType > other.handType)
            return 1;
        if (handType < other.handType)
            return -1;
        if (handValue > other.handValue)
            return 1;
        if (handValue < other.handValue)
            return -1;
        return 0;
    }

    private void generateDensity() {
        cardDensity = new HashMap<Integer, Integer>();
        Integer a;
        for (int i = 0; i < size; i++) {
            a = cardDensity.putIfAbsent(cardVals.get(i), 1);
            if (a != null)
                cardDensity.put(cardVals.get(i), cardDensity.get(cardVals.get(i)) + 1);
        }
    }

    private void sortVals() {
        Integer vTemp;
        Integer sTemp;
        boolean sorted = false;

        while (!sorted) {
            sorted = true;
            for (int i = 0; i < size - 1; i++) {
                if (cardVals.get(i).compareTo(cardVals.get(i + 1)) < 0) {
                    vTemp = cardVals.get(i);
                    cardVals.set(i, cardVals.get(i + 1));
                    cardVals.set(i + 1, vTemp);
                    sTemp = cardSuits.get(i);
                    cardSuits.set(i, cardSuits.get(i + 1));
                    cardSuits.set(i + 1, sTemp);
                    sorted = false;
                }
            }
        }
    }

    private int getStraightFlush() {
        int temp = checkStrFl(1);
        if (temp != -1)
            return temp;
        temp = checkStrFl(2);
        if (temp != -1)
            return temp;
        temp = checkStrFl(3);
        if (temp != -1)
            return temp;

        if (cardVals.get(0) == 14) {
            boolean strFl = false;
            int firstVal = cardVals.get(size - 1);
            if (firstVal != 2)
                return -1;
            int lastVal = firstVal;
            int suit = cardSuits.get(size - 1);
            for (int i = size - 2; i > 0; i--) {
                if (cardVals.get(i) == lastVal)
                    continue;
                if (cardSuits.get(i) != suit)
                    break;
                if (cardVals.get(i) > lastVal + 1)
                    break;
                if (cardVals.get(i) == firstVal + 3) {
                    strFl = true;
                    break;
                }
                lastVal = cardVals.get(i);
            }
            if (strFl) {
                return 1;
            }
        }

        return -1;
    }

    private int checkStrFl(int dist) {
        boolean strFl = false;
        int firstVal = cardVals.get(dist - 1);
        int lastVal = firstVal;
        int suit = cardSuits.get(dist - 1);
        for (int i = dist; i < size; i++) {
            if (cardVals.get(i) == lastVal)
                continue;
            if (cardSuits.get(i) != suit)
                break;
            if (cardVals.get(i) < lastVal - 1)
                break;
            if (cardVals.get(i) == firstVal - 4) {
                strFl = true;
                break;
            }
            lastVal = cardVals.get(i);
        }
        if (strFl) {
            return firstVal - 4;
        }
        return -1;
    }

    private int get4OfKind() {
        int fourVal = 0;
        List<Integer> fours =
                cardDensity.entrySet().stream().filter(e -> e.getValue() == 4).map(Map.Entry::getKey)
                        .collect(Collectors.toList());
        if (fours.size() == 1)
            fourVal = fours.get(0);
        else
            return -1;
        for (Integer i : cardVals) {
            if (i == fourVal)
                continue;
            return 15 * fourVal + i;
        }
        return 15 * fourVal;
    }

    private int getFullHouse() {
        List<Integer> trips =
                cardDensity.entrySet().stream().filter(e -> e.getValue() == 3).map(Map.Entry::getKey)
                        .collect(Collectors.toList());
        List<Integer> pairs =
                cardDensity.entrySet().stream().filter(e -> e.getValue() == 2).map(Map.Entry::getKey)
                        .collect(Collectors.toList());
        if (trips.size() > 0 && pairs.size() > 0)
            return 15 * trips.get(0) + max(pairs);
        return -1;
    }

    private int getFlush() {
        List<List<Integer>> suits = new ArrayList<List<Integer>>();
        for (int i = 0; i < 4; i++)
            suits.add(new ArrayList<Integer>());
        for (int i = 0; i < size; i++) {
            suits.get(cardSuits.get(i)).add(cardVals.get(i));
        }
        for (int i = 0; i < 4; i++) {
            if (suits.get(i).size() > 4) {
                List<Integer> flush = suits.get(i);
                return 15 * 15 * 15 * 15 * flush.get(0) + 15 * 15 * 15 * flush.get(1) + 15 * 15 * flush.get(2)
                        + 15 * flush.get(3) + flush.get(4);
            }
        }
        return -1;
    }

    private int getStraight() {
        int temp = checkStr(1);
        if (temp != -1)
            return temp;
        temp = checkStr(2);
        if (temp != -1)
            return temp;
        temp = checkStr(3);
        if (temp != -1)
            return temp;

        if (cardVals.get(0) == 14) {
            boolean str = false;
            int firstVal = cardVals.get(size - 1);
            if (firstVal != 2)
                return -1;
            int lastVal = firstVal;
            for (int i = size - 2; i > 0; i--) {
                if (cardVals.get(i) == lastVal)
                    continue;
                if (cardVals.get(i) > lastVal + 1)
                    break;
                if (cardVals.get(i) == firstVal + 3) {
                    str = true;
                    break;
                }
                lastVal = cardVals.get(i);
            }
            if (str) {
                return 1;
            }
        }

        return -1;
    }

    private int checkStr(int dist) {
        boolean str = false;
        int firstVal = cardVals.get(dist - 1);
        int lastVal = firstVal;
        for (int i = dist; i < size; i++) {
            if (cardVals.get(i) == lastVal)
                continue;
            if (cardVals.get(i) < lastVal - 1)
                break;
            if (cardVals.get(i) == firstVal - 4) {
                str = true;
                break;
            }
            lastVal = cardVals.get(i);
        }
        if (str) {
            return firstVal - 4;
        }
        return -1;
    }

    private int get3OfKind() {
        int threeVal = 0;
        List<Integer> trips =
                cardDensity.entrySet().stream().filter(e -> e.getValue() == 3).map(Map.Entry::getKey)
                        .collect(Collectors.toList());
        if (trips.size() > 0)
            threeVal = trips.get(0);
        else
            return -1;
        int kicker = 0;
        for (Integer i : cardVals) {
            if (kicker == 0 && i != threeVal)
                kicker = i;
            else if (kicker != 0 && i != kicker && i != threeVal)
                return 15 * 15 * threeVal + 15 * kicker + i;

        }
        return 15 * 15 * threeVal + 15 * kicker;
    }

    private int get2Pair() {
        int highPair = 0;
        int lowPair = 0;
        List<Integer> pairs =
                cardDensity.entrySet().stream().filter(e -> e.getValue() == 2).map(Map.Entry::getKey)
                        .collect(Collectors.toList());
        if (pairs.size() > 1) {
            highPair = Math.max(pairs.get(0), pairs.get(1));
            lowPair = Math.min(pairs.get(0), pairs.get(1));
        }
        else
            return -1;

        for (Integer i : cardVals) {
            if (i == 0)
                break;
            if (i != highPair && i != lowPair)
                return 15 * 15 * highPair + 15 * lowPair + i;
        }
        return 15 * 15 * highPair + 15 * lowPair;

    }

    private int getPair() {
        int pair = 0;
        List<Integer> pairs =
                cardDensity.entrySet().stream().filter(e -> e.getValue() == 2).map(Map.Entry::getKey)
                        .collect(Collectors.toList());
        if (pairs.size() > 0) {
            pair = pairs.get(0);
        }
        else
            return -1;
        int kicker = 0, kicker2 = 0;
        for (Integer i : cardVals) {
            if (i == 0)
                break;
            if (kicker == 0 && pair != i)
                kicker = i;
            else if (kicker != 0 && kicker2 == 0 && pair != i && kicker != i)
                kicker2 = i;
            else if (kicker != 0 && kicker2 != 0 && pair != i && kicker != i && kicker2 != i)
                return 15 * 15 * 15 * pair + 15 * 15 * kicker + 15 * kicker2 + i;
        }
        return 15 * 15 * 15 * pair + 15 * 15 * kicker + 15 * kicker2;

    }


    private int getHighCard() {
        int sum = 0;
        for (int i = 0; i < 5 && i < size; i++) {
            sum += (int) Math.pow(15, 4 - i) * cardVals.get(i);
        }
        return sum;
    }

    public String toString() {
        switch (handType) {
            case 8:
                return getStraightFlushStr();
            case 7:
                return get4OfKindStr();
            case 6:
                return getFullHouseStr();
            case 5:
                return getFlushStr();
            case 4:
                return getStraightStr();
            case 3:
                return get3OfKindStr();
            case 2:
                return get2PairStr();
            case 1:
                return getPairStr();
            default:
                return getHighCardStr();
        }
    }

    private static String getStringValue(int value) {
        if (value == 1)
            return "A";
        if (value < 11)
            return String.valueOf(value);
        switch (value) {
            case 11:
                return "J";
            case 12:
                return "Q";
            case 13:
                return "K";
            default:
                return "A";
        }
    }

    private static List<Integer> getBase15PlaceValues(int base15Num) {
        List<Integer> placeValues = new ArrayList<Integer>();
        while (base15Num > 0) {
            if (base15Num % 15 != 0)
                placeValues.add(0, base15Num % 15);
            base15Num /= 15;
        }

        return placeValues;
    }

    private String getStraightFlushStr() {
        return "Straight Flush from " + getStringValue(handValue) + " to " + getStringValue(handValue + 4);
    }

    private String get4OfKindStr() {
        List<Integer> vals = getBase15PlaceValues(handValue);
        if (vals.size() > 1)
            return "[4 of a Kind of " + getStringValue(vals.get(0)) + "] + " + getStringValue(vals.get(1));
        return "4 of a Kind of " + getStringValue(vals.get(0));
    }

    private String getFullHouseStr() {
        List<Integer> vals = getBase15PlaceValues(handValue);
        return "Full House: [Three of a Kind of " + getStringValue(vals.get(0)) + "] + [Pair of " +
                getStringValue(vals.get(1)) + "]";
    }

    private String getFlushStr() {
        List<Integer> vals = getBase15PlaceValues(handValue);
        StringBuilder sb = new StringBuilder();
        sb.append("Flush with ");
        sb.append(getStringValue(vals.get(0)));
        for (int i = 1; i < vals.size(); i++) {
            sb.append(", ");
            sb.append(getStringValue(vals.get(i)));
        }
        return sb.toString();
    }

    private String getStraightStr() {
        return "Straight from " + getStringValue(handValue) + " to " + getStringValue(handValue + 4);
    }

    private String get3OfKindStr() {
        List<Integer> vals = getBase15PlaceValues(handValue);
        StringBuilder sb = new StringBuilder();
        sb.append("Three of a Kind of ");
        sb.append(getStringValue(vals.get(0)));
        sb.append("]");
        if (vals.size() < 2)
            return sb.toString();
        sb.append(" + ");
        sb.append(getStringValue(vals.get(1)));
        for (int i = 2; i < vals.size(); i++) {
            sb.append(", ");
            sb.append(getStringValue(vals.get(i)));
        }
        return sb.toString();
    }

    private String get2PairStr() {
        List<Integer> vals = getBase15PlaceValues(handValue);
        StringBuilder sb = new StringBuilder();
        sb.append("2 Pair: [");
        sb.append(getStringValue(vals.get(0)));
        sb.append(" Pair] + [");
        sb.append(getStringValue(vals.get(1)));
        sb.append(" Pair]");
        if (vals.size() < 3)
            return sb.toString();
        sb.append(" + ");
        sb.append(getStringValue(vals.get(2)));
        return sb.toString();
    }

    private String getPairStr() {
        List<Integer> vals = getBase15PlaceValues(handValue);
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(getStringValue(vals.get(0)));
        sb.append(" Pair]");
        if (vals.size() < 2)
            return sb.toString();
        sb.append(" + ");
        sb.append(getStringValue(vals.get(1)));
        for (int i = 2; i < vals.size(); i++) {
            sb.append(", ");
            sb.append(getStringValue(vals.get(i)));
        }
        return sb.toString();
    }

    private String getHighCardStr() {
        List<Integer> vals = getBase15PlaceValues(handValue);
        if (vals.size() == 0)
            return "No cards in hand";
        StringBuilder sb = new StringBuilder();
        sb.append("High Card with ");
        sb.append(getStringValue(vals.get(0)));
        for (int i = 1; i < vals.size(); i++) {
            sb.append(", ");
            sb.append(getStringValue(vals.get(i)));
        }
        return sb.toString();
    }

    private static int max(List<Integer> a) {
        int max = 0;
        for (Integer i : a)
            max = Math.max(max, i);
        return max;
    }

    public int getHandType()
    {
        return handType;
    }

}