package org.oldmonksxis.gameengine.errors;

public class CommandRejectedException extends RuntimeException {
    public CommandRejectedException(String message) {
        super(message);
    }
}
