package org.oldmonksxis.resources;

import org.oldmonksxis.api.*;
import org.oldmonksxis.gameengine.GameRoom;
import org.oldmonksxis.gameengine.BettingRound;
import org.oldmonksxis.gameengine.PlayerState;
import org.oldmonksxis.gameengine.errors.CommandRejectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("/api/player")
@Produces(MediaType.APPLICATION_JSON)
public class PlayerApiResource {

    private static final Logger logger = LoggerFactory.getLogger(PlayerApiResource.class);
    private static final String PlayerCookieName = "player-secret-token";
    private final GameRoom gameRoom;
    private boolean isFakeGame;

    public PlayerApiResource(GameRoom gameRoom, boolean isFakeGame) {
        this.gameRoom = gameRoom;
        this.isFakeGame = isFakeGame;
    }

    @POST
    @Path("/register")
    public PlayerRegistrationResponse register(PlayerRegistrationRequest playerRegistrationRequest) {
        String playerSecretToken = null;
        PlayerRegistrationResponse playerRegistrationResponse = new PlayerRegistrationResponse();

        try {
            playerSecretToken = gameRoom.getRegistration().playerRegister(playerRegistrationRequest.name);
            logger.info("Player '{}', generated token {}", playerRegistrationRequest.name, playerSecretToken);
            playerRegistrationResponse.success = true;
            playerRegistrationResponse.playerSecretToken = playerSecretToken;
        } catch (CommandRejectedException e) {
            playerRegistrationResponse.success = false;
            playerRegistrationResponse.error = e.getMessage();
            logger.error("Failed to generate token for '{}', error: {}",
                    playerRegistrationRequest.name, e.getMessage());
        }

        return playerRegistrationResponse;
    }

    @GET
    @Path("/state")
    public PlayerStateResponse getState(@CookieParam(PlayerCookieName) Cookie playerSecretTokenCookie) {
        // TODO: Run this inside a tryRun
        int playerIdx = getPlayerIdx(playerSecretTokenCookie);
        return gameRoom.getBettingRound().getPlayerStateResponse(playerIdx);
    }

    @POST
    @Path("/bet")
    public GenericActionResponse bet(
            @CookieParam(PlayerCookieName) Cookie playerSecretTokenCookie,
            PlaceBetRequest placeBetRequest) {
        return tryRun(() -> {
            int playerIdx = getPlayerIdx(playerSecretTokenCookie);
            gameRoom.getBettingRound().playerPlaceBet(playerIdx, placeBetRequest.money);
        });
    }

    @POST
    @Path("/fold")
    public GenericActionResponse fold(@CookieParam(PlayerCookieName) Cookie playerSecretTokenCookie) {
        return tryRun(() -> {
            int playerIdx = getPlayerIdx(playerSecretTokenCookie);
            gameRoom.getBettingRound().fold(playerIdx);
        });
    }

    @GET
    @Path("/poll/{gameStateSequenceNumber}")
    public StateChangeNotification pollForStateChanged(
            @CookieParam(PlayerCookieName) Cookie playerSecretTokenCookie,
            @PathParam("gameStateSequenceNumber") int gameStateSequenceNumber) {

        // Polling is player agnostic - just check the cookie for authorization
        // If it's a fake game, then the player cookie is returned on the first
        // poll attempt and the browser sets it (to make development easier)
        int playerIdx = getPlayerIdxNonThrowing(playerSecretTokenCookie);
        if (!isFakeGame && playerIdx < 0) return new StateChangeNotification("Unauthorized, invalid cookie");

        BettingRound bettingRound = gameRoom.getBettingRound();
        if (bettingRound == null) return new StateChangeNotification("Game not started yet");
        int newGameStateSeqNumber = bettingRound.waitForGameStateChange(gameStateSequenceNumber);

        // if it is a fake game, then we return the cookie on the first poll response
        String cookieForFakeGame = null;
        if (isFakeGame && playerIdx < 0) {
            cookieForFakeGame = gameRoom.getRegistration().getNextCookieForFakeGame();
            logger.info("Returning cookie for player {} for fake game", getPlayer(cookieForFakeGame).getName());
        }
        return new StateChangeNotification(newGameStateSeqNumber, isFakeGame, cookieForFakeGame);
    }

    private PlayerState getPlayer(String playerSecretToken) {
        return gameRoom.getBettingRound().getPlayerStates().get(getPlayerIdx(playerSecretToken));
    }

    private int getPlayerIdx(Cookie playerSecretTokenCookie) {
        String playerSecretToken = playerSecretTokenCookie.getValue();
        return getPlayerIdx(playerSecretToken);
    }

    private int getPlayerIdx(String playerSecretToken) {
        BettingRound bettingRound = gameRoom.getBettingRound();
        if (bettingRound == null) throw new CommandRejectedException("Game not started yet");
        return bettingRound.getPlayerIndex(playerSecretToken);
    }

    private int getPlayerIdxNonThrowing(Cookie playerSecretToken) {
        BettingRound bettingRound = gameRoom.getBettingRound();
        if (bettingRound == null) return -1;
        try {
            if (playerSecretToken == null) return -1;
            return bettingRound.getPlayerIndex(playerSecretToken.getValue());
        } catch (CommandRejectedException e) {
            return -1;
        }
    }

    private static GenericActionResponse tryRun(Runnable runnable) {
        try {
            runnable.run();
            return new GenericActionResponse();
        } catch (CommandRejectedException e) {
            GenericActionResponse r = new GenericActionResponse();
            r.success = false;
            r.error = e.getMessage();
            return r;
        }
    }
}
