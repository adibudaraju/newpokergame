package org.oldmonksxis.resources;

import org.oldmonksxis.api.GenericActionResponse;
import org.oldmonksxis.api.PlayersInRoomResponse;
import org.oldmonksxis.gameengine.GameRoom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/admin")
@Produces(MediaType.APPLICATION_JSON)
public class AdminApiResource {
    private static final Logger logger = LoggerFactory.getLogger(AdminApiResource.class);
    private static final String AdminSecretToken = "admin-secret-token";
    private final GameRoom gameRoom;
    private String adminSecret;
    private boolean devMode;

    public AdminApiResource(GameRoom gameRoom, String adminSecret, boolean devMode, boolean generateFakeGame) {
        this.gameRoom = gameRoom;
        this.adminSecret = adminSecret;
        this.devMode = devMode;
        if (generateFakeGame) {
            for (int i = 1; i <= 5; i++) {
                gameRoom.getRegistration().playerRegister("Player " + i);
            }
            gameRoom.startGame();
        }
    }

    @GET
    @Path("/listplayers")
    public Response listPlayers(@CookieParam(AdminSecretToken) Cookie adminSecretToken) {
        if (!devMode && (adminSecretToken == null || !adminSecretToken.getValue().equals(adminSecret)))
            return Response.status(Response.Status.FORBIDDEN).build();
        PlayersInRoomResponse playersInRoomResponse = new PlayersInRoomResponse();
        playersInRoomResponse.playerNames = gameRoom.getRegistration().listPlayerNames();
        return Response.ok().entity(playersInRoomResponse).build();
    }

    @POST
    @Path("/startgame")
    public Response startGame(@CookieParam(AdminSecretToken) Cookie playerSecretTokenCookie) {
        if (!devMode && (!playerSecretTokenCookie.getValue().equals(adminSecret)))
            return Response.status(Response.Status.FORBIDDEN).build();
        GenericActionResponse resp = new GenericActionResponse();
        if (gameRoom.getBettingRound() != null) {
            resp.error = "Game already in progress";
            return Response.ok().entity(resp).build();
        }
        gameRoom.startGame();
        resp.success = true;
        return Response.ok().entity(resp).build();
    }
}
