package org.oldmonksxis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.lang.Thread.sleep;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class DevmodeStaticFilesSync {
    static Logger logger = LoggerFactory.getLogger(DevmodeStaticFilesSync.class);
    private final File sourceDir;
    private final File destDir;
    private ScheduledExecutorService sx;

    public DevmodeStaticFilesSync(String sourceDirPath, String destDirPath) {
        sourceDir = new File(sourceDirPath);
        destDir = new File(destDirPath);
    }

    public void watch() {
        sx = Executors.newScheduledThreadPool(1);
        sx.scheduleAtFixedRate((Runnable) () -> sync(), 1, 1, TimeUnit.SECONDS);
    }

    public void sync() {
        ArrayList<File> sourceFiles = list(sourceDir);
        ArrayList<File> destFiles = list(destDir);
        for (File sourceFile : sourceFiles) {
            List<File> matchedFiles = destFiles.stream().filter(d -> d.getName().equals(sourceFile.getName())).collect(Collectors.toList());
            if (matchedFiles.isEmpty()) {
                logger.info("Missing file {}", sourceFile.getName());
                continue;
            }
            File matchedFile = matchedFiles.get(0);
            if (matchedFile.lastModified() < sourceFile.lastModified()) {
                logger.info("File {} out of date", sourceFile.getName());
                try {
                    logger.info("Copying {} -> {}", sourceFile, matchedFile);
                    Files.copy(sourceFile.toPath(), matchedFile.toPath(), REPLACE_EXISTING);
                } catch (IOException e) {
                    logger.error("Unable to copy file", e);
                }
            }
        }
    }

    public static ArrayList<File> list(File file) {
       ArrayList<File> children = new ArrayList<>();
       walk(file, children);
       return children;
    }

    public static void walk(File file, ArrayList<File> files) {
        if(file.isHidden())
            return;
        if (!file.isDirectory()) {
           files.add(file);
           return;
        }
        File[] children = file.listFiles();
        for (File child : children) {
            walk(child, files);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Path currentPath = Paths.get(System.getProperty("user.dir"));
        String assetStr = Paths.get(currentPath.toString(), "src", "main", "resources", "assets").toString();
        String targetStr = Paths.get(currentPath.toString(), "target", "classes", "assets").toString();
        DevmodeStaticFilesSync sync = new DevmodeStaticFilesSync(assetStr, targetStr);
        sync.watch();
        sleep(1000000000);
    }
}
