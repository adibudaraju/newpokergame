package org.oldmonksxis;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.oldmonksxis.core.Secret;
import org.oldmonksxis.gameengine.GameRoom;
import org.oldmonksxis.resources.AdminApiResource;
import org.oldmonksxis.resources.PlayerApiResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class PokerServerApplication extends Application<PokerserverConfiguration> {

    static Logger logger = LoggerFactory.getLogger(PokerServerApplication.class);

    public static void main(final String[] args) throws Exception {
        // TODO: Only in dev-mode
        var currentPath = Paths.get(System.getProperty("user.dir"));
        var srcPath = Paths.get(currentPath.toString(), "src", "main", "resources", "assets").toString();
        var destPath = Paths.get(currentPath.toString(), "target", "classes", "assets").toString();
        var sync = new DevmodeStaticFilesSync(srcPath, destPath);
        sync.watch();
        new PokerServerApplication().run(args);
    }

    @Override
    public String getName() {
        return "pokerserver";
    }

    @Override
    public void initialize(final Bootstrap<PokerserverConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle());
    }

    @Override
    public void run(final PokerserverConfiguration configuration,
                    final Environment environment) {
        String adminSecret = Secret.generateSecretToken(32);
        System.out.println("Copy the admin secret below");
        System.out.printf("Admin secret: %s%n", adminSecret);

        // TODO: make devmode configurable
        String generateFakeGamePropVal = System.getProperty("fakegame");
        boolean generateFakeGame = generateFakeGamePropVal != null && generateFakeGamePropVal.isEmpty();
        if (generateFakeGame) logger.info("Generating fake game for testing");
        GameRoom gameRoom = new GameRoom(generateFakeGame);
        environment.jersey().register(new AdminApiResource(gameRoom, adminSecret, true, generateFakeGame));
        environment.jersey().register(new PlayerApiResource(gameRoom, true));
    }
}
