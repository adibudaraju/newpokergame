package org.oldmonksxis.api;

public class GenericActionResponse {
    public boolean success = true;
    public String error;

    public GenericActionResponse() {}
    public GenericActionResponse(String error) {
        this.error = error;
        this.success = false;
    }
}
