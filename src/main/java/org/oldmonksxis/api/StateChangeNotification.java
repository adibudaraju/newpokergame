package org.oldmonksxis.api;

public class StateChangeNotification extends GenericActionResponse {
    public int gameStateCounter;
    public boolean isFakeGame;
    public String playerSecretCookieIfFakeGame;

    public StateChangeNotification(
            int gameStateCounter,
            boolean isFakeGame,
            String playerSecretCookieIfFakeGame) {

        super();
        this.success = true;
        this.gameStateCounter = gameStateCounter;
        this.isFakeGame = isFakeGame;
        this.playerSecretCookieIfFakeGame = playerSecretCookieIfFakeGame;
    }

    public StateChangeNotification(String errorMessage) {
        this.success = false;
        this.error = errorMessage;
    }
}
