package org.oldmonksxis.api;

import org.oldmonksxis.gameengine.Hand;

import java.util.List;

public class PlayerStateResponse {
    // Public state of other players visible to all players
    public static class PlayerStatePublic {
        public String name;
        public int money;
        public int moneyInPot;
        public int totalMoneyInPot;
        public boolean hasFolded;
        public boolean isOnTurn;
        public boolean isDealer;
        public boolean isAllIn;
        public String avatarUri;

        /**
         *  Strictly speaking - this is the player's private cookie - but if we are running a fake
         *  game, we will provide it to the browser so the tester can switch player identities
         *  easily.
         */
        public String cookieDuringFakeGame;

        public PlayerStatePublic(
                String name,
                int money,
                int moneyInPot,
                int totalMoneyInPot,
                boolean hasFolded,
                boolean isOnTurn,
                boolean isDealer,
                boolean isAllIn,
                String avatarUri,
                String cookieDuringFakeGame) {

            this.name = name;
            this.money = money;
            this.moneyInPot  =  moneyInPot;
            this.totalMoneyInPot = totalMoneyInPot;
            this.hasFolded = hasFolded;
            this.isOnTurn = isOnTurn;
            this.isDealer = isDealer;
            this.isAllIn = isAllIn;
            this.avatarUri = avatarUri;
            this.cookieDuringFakeGame = cookieDuringFakeGame;
        }
    }

    // State of player visible only to themselves, either secrets,
    // or information that only they care about (e.g. for the purpose
    // of display to their browser)
    public static class PlayerStatePrivate {
        public final String name;
        public int money;
        public int  moneyInPot;
        public int totalMoneyInPot;
        public Hand bestHandSoFar;
        public List<String> cardFileUris;
        public List<String> communityCardUris;
        public int gameStateSequenceNumber;
        public List<String> stateChangeMessages;
        public String whoHasTheTurn;
        public String dealer;
        public boolean isOnTurn;
        public boolean isDealer;
        public boolean isAllIn;
        public boolean hasFolded;

        public PlayerStatePrivate(
                String name,
                int money,
                int moneyInPot,
                int totalMoneyInPot,
                Hand bestHandSoFar,
                List<String> cardFileUris,
                List<String> communityCardUris,
                int gameStateSequenceNumber,
                List<String> stateChangeMessages,
                String whoHasTheTurn,
                String dealer,
                boolean isOnTurn,
                boolean isDealer,
                boolean isAllIn,
                boolean hasFolded) {

            this.name = name;
            this.money = money;
            this.moneyInPot =  moneyInPot;
            this.totalMoneyInPot = totalMoneyInPot;
            this.bestHandSoFar = bestHandSoFar;
            this.cardFileUris = cardFileUris;
            this.communityCardUris = communityCardUris;
            this.gameStateSequenceNumber = gameStateSequenceNumber;
            this.stateChangeMessages = stateChangeMessages;
            this.whoHasTheTurn = whoHasTheTurn;
            this.dealer = dealer;
            this.isOnTurn = isOnTurn;
            this.isDealer = isDealer;
            this.isAllIn = isAllIn;
            this.hasFolded = hasFolded;
        }
    }

    public PlayerStatePrivate playerStatePrivate;
    public List<PlayerStatePublic> playerStatesPublic;
    public List<String> messages;

    public PlayerStateResponse(
            PlayerStatePrivate playerStatePrivate,
            List<PlayerStatePublic> publicStates) {

        this.playerStatePrivate = playerStatePrivate;
        this.playerStatesPublic = publicStates;
    }
}
