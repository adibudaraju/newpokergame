package org.oldmonksxis;


import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/pokermsgq")
public class PokerWebSocketServlet extends WebSocketServlet {
    static Logger logger = LoggerFactory.getLogger(PokerWebSocketServlet.class);

    @Override
    public void configure(WebSocketServletFactory webSocketServletFactory) {
       webSocketServletFactory.getPolicy().setIdleTimeout(10000);
       webSocketServletFactory.register(PokerWebSocket.class);
       logger.info("Registered websocket");
    }

    /*
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Received POST");
        super.doPost(req, resp);
        String test = "Millis: " + System.currentTimeMillis();
        resp.getOutputStream().write(test.getBytes());
    }
     */
}
