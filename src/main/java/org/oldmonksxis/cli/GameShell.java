package org.oldmonksxis.cli;

import org.oldmonksxis.gameengine.BettingRound;
import org.oldmonksxis.gameengine.PlayerState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class GameShell {
    static Logger logger = LoggerFactory.getLogger(GameShell.class);
    private final BettingRound bettingRound;
    private final ArrayList<PlayerAgent> playerAgents;
    private final ArrayList<Thread> playerThreads;

    public static void main(String[] args) throws IOException {
        ArrayList<String> somePlayers = new ArrayList<>();
        somePlayers.add("A");
        somePlayers.add("B");
        somePlayers.add("C");
        somePlayers.add("D");
        somePlayers.add("E");

        GameShell gameShell = new GameShell(somePlayers, 20);
        if (args.length == 0) {
            gameShell.interactiveLoop();
        } else if (args.length == 1) {
            gameShell.executeFileCommands(args[0]);
        }
        gameShell.shutdown();
    }

    public GameShell(List<String> playerNames, int moneyPerPlayer) {
        List<PlayerState> playerStates = playerNames.stream()
                .map(p -> new PlayerState(moneyPerPlayer, p, p, p))
                .collect(Collectors.toList());
        bettingRound = new BettingRound(playerStates, moneyPerPlayer, false); // make this false when running
        playerAgents = new ArrayList<>();
        playerThreads = new ArrayList<>();

        for (int playerIdx = 0; playerIdx < playerNames.size(); playerIdx++) {
            PlayerAgent playerAgent = new PlayerAgent(bettingRound, playerIdx);
            Thread playerThread = new Thread(playerAgent, playerNames.get(playerIdx));
            playerThread.start();
            playerAgents.add(playerAgent);
            playerThreads.add(playerThread);
        }
    }

    private void interactiveLoop() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.printf("> ");
            String command = br.readLine();
            runCommand(command);
        }
    }

    private void executeFileCommands(String filePath) throws IOException {
        logger.info("Executing commands from {}", filePath);
        FileInputStream fis = new FileInputStream(filePath);
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);
        while (true) {
            String command = br.readLine();
            if (command == null) break;
            logger.info("Running command: {}", command);
            runCommand(command);
            System.out.println();
            System.out.println();
        }
    }

    private void runCommand(String command) {

        Pattern cmdRegex = Pattern.compile("(?<playerid>\\d+)(?<cmd>\\s*.*)");
        Matcher matcher = cmdRegex.matcher(command);

        // Player command
        if (matcher.matches()) {
            int playerid = Integer.parseInt(matcher.group("playerid"));
            String cmd = matcher.group("cmd").trim();
            logger.info("Running command for player {}", playerid);
            PlayerAgent playerAgent = playerAgents.get(playerid);
            playerAgent.runCommand(cmd);
            return;
        }

        // admin command
        if (command.equals("q")) {
            logger.info("Exiting");
            return;
        }

        if (command.equals("dump")) {
            logger.info("Game state counter: {}", bettingRound.getGameStateCounter());
            logger.info("Player on turn: {}", bettingRound.getPlayerOnTurnIdx());
            logger.info("Pot: {}", bettingRound.getMoneyInPot());
            logger.info("Dealer index: {}", bettingRound.getDealerPlayerIdx());
            logger.info("Community cards: {}", bettingRound.getCommunityCardFileNames());
            for (PlayerState playerState : bettingRound.getPlayerStates()) {
                playerState.dump(BettingRound.CardImageFileNamesInOrder);
            }
            return;
        }

        logger.error("Invalid command: '{}'", command);
    }

    private void shutdown() {
        playerAgents.stream().forEach(p -> p.doShutdown());
        playerThreads.stream().forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
