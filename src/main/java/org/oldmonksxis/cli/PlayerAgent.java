package org.oldmonksxis.cli;

import org.oldmonksxis.gameengine.BettingRound;
import org.oldmonksxis.gameengine.errors.CommandRejectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlayerAgent implements Runnable {
    static final Logger logger = LoggerFactory.getLogger(PlayerAgent.class);
    private BettingRound bettingRound;
    private int playerIdx;
    private String currentCommand;
    private boolean shutdown;

    public PlayerAgent(BettingRound bettingRound, int playerIdx) {
        this.bettingRound = bettingRound;
        this.playerIdx = playerIdx;
    }

    @Override
    public synchronized void run() {
        while (true) {
            // Wait for a command
            try {
                wait();
            } catch (InterruptedException e) {
               throw new RuntimeException(e);
            }

            if (shutdown) {
                logger.info("Player {} agent shutting down", playerIdx);
                return;
            }

            try {
                dispatchCommand();
            } catch (CommandRejectedException e) {
                logger.error("Command rejected: {}", e.getMessage());
            }
            logger.info("Command finished");
            currentCommand = null;
        }
    }

    private void dispatchCommand() {

        if (currentCommand.equals("cards")) {
            for (String card : bettingRound.getPlayerCards(playerIdx)) {
                logger.info("Player {} card -> {}", playerIdx, card);
            }
            return;
        }

        if (currentCommand.equals("hand")) {
            logger.info("Player {} best hand: {}", playerIdx,
                    bettingRound.getPlayerStates().get(playerIdx).getBestHandSoFar().toString());
            return;
        }

        if (currentCommand.equals("fold")) {
            this.bettingRound.fold(playerIdx);
            return;
        }

        if (currentCommand.equals("show")) {
            this.bettingRound.show(playerIdx);
            return;
        }

        if(currentCommand.equals("contributions"))
        {
            this.bettingRound.shellDebugContributions();
            return;
        }

        if(currentCommand.equals("totals"))
        {
            this.bettingRound.shellDebugTotals();
            return;
        }

        if(currentCommand.equals("money"))
        {
            this.bettingRound.shellDebugMoneys();
            return;
        }

        if (currentCommand.equals("waitforchange")) {
            int currentGameStateCounter = this.bettingRound.getGameStateCounter();
            logger.info("Player {} waiting for game state change from {}", playerIdx, currentGameStateCounter);
            this.bettingRound.waitForGameStateChange(currentGameStateCounter);
            currentGameStateCounter = this.bettingRound.getGameStateCounter();
            logger.info("Player {} received game state change, new counter is {}", playerIdx, currentGameStateCounter);
            return;
        }

        if (currentCommand.startsWith("bet ")) {
            Pattern pattern = Pattern.compile("bet\\s+(?<amount>\\d+)");
            Matcher matcher = pattern.matcher(currentCommand);
            if (!matcher.matches()) {
                logger.error("Bet needs amount");
            }
            int amount = Integer.parseInt(matcher.group("amount"));
            bettingRound.playerPlaceBet(playerIdx, amount);
            return;
        }
        logger.info("Unknown command");
    }

    public void runCommand(String cmd) {
        queueCommand(cmd);
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized void queueCommand(String cmd) {
        if (this.currentCommand != null) {
           logger.error("Player has not yet processed their current command");
        }
        this.currentCommand = cmd;
        notify();
    }

    public synchronized void doShutdown() {
        shutdown = true;
        notify();
    }
}
