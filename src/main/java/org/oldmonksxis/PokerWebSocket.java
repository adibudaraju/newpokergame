package org.oldmonksxis;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import java.io.IOException;

@WebSocket
public class PokerWebSocket {
    @OnWebSocketConnect
    public void onConnect(Session session) {
        try {
            session.getRemote().sendString("Hello");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
